package readF;


import binaryFile.*;

import java.io.*;

class BinaryFile {

    public void createBinaryFile() throws IOException {
        Page page = new Page("Page1", 210, 297, new Node[]{
                new FlowArea("FlowArea1", 10, 10, 80, 50, new String[]{"Petr Novak", "Jiraskova 213", "Brno", "12345"}),
                new FlowArea("FlowArea2", 120, 10, 80, 50, new String[]{"Moje firma", "Product manager", "Ing. Jana Novotna", "Hradecka 25", "Pardubice", "54332", "Tel: 123456789"}),
                new Rectangle("Rectangle1", 5, 70, 200, 2, new Color("binaryFile.Color", 0, 0, 0)),
                new FlowArea("FlowArea3", 10, 80, 190, 190, new String[]{"Line 1", "Line 2", "Line 3", "Line 4", "Line 5"}),
        });     FileOutputStream fos = new FileOutputStream("data.data");
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(page);
        oos.close();
    }

    public void readBinaryFile() throws IOException, ClassNotFoundException {
        File file = new File("data.data");
        FileInputStream fis = new FileInputStream(file);
        ObjectInputStream ois = new ObjectInputStream(fis);
        Node object = (Node) ois.readObject();
        System.out.println(object.toString());
    }

}