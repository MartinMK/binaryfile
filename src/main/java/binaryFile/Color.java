package binaryFile;

import java.io.*;

public class Color extends Node implements Serializable {
    private double r;
    private double g;
    private double b;
private String name ;
    public Color() {
    }

    public Color(String name, double r, double g, double b) {
        this.name = name;
        this.r = r;
        this.g = g;
        this.b = b;
    }

    @Override
    public String toString() {
        return "\n" + "binaryFile.Color" + "\n" +
                "- R :" + r + "\n" +
                "- G :" + g + "\n" +
                "- B :" + b + "\n";
    }

    @Override
    public void save(RandomAccessFile dataOutputStream) throws IOException {
        super.save(dataOutputStream);
        dataOutputStream.writeUTF(name);
        dataOutputStream.writeDouble(r);
        dataOutputStream.writeDouble(g);
        dataOutputStream.writeDouble(b);
    }

    @Override
    public void load(RandomAccessFile dataInputStream) throws IOException {
        super.load(dataInputStream);
       name = dataInputStream.readUTF();
        r = dataInputStream.readDouble();
        g = dataInputStream.readDouble();
        b = dataInputStream.readDouble();
    }
}
