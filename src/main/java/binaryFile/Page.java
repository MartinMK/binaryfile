package binaryFile;

import java.io.*;
import java.util.Arrays;

public class Page extends Node implements Serializable {
    private double width;
    private double height;
    private Node[] nodeRef;
    private String name;
    private long nextPosition;

    public Page() {
    }

    public Page(String name, double width, double height, Node[] nodeRef) {
        this.name = name;
        this.width = width;
        this.height = height;
        this.nodeRef = nodeRef;
    }

    @Override
    public String toString() {
        return     "name=" + name + "\n" +
                "width=" + width + "\n" +
                        "height=" + height + "\n" +
                        "nodeRef=" + Arrays.toString(nodeRef) + "\n"
                ;
    }


    @Override
    public void save(RandomAccessFile dataOutputStream) throws IOException {
        long position = dataOutputStream.getFilePointer();
        dataOutputStream.writeLong(position);
        super.save(dataOutputStream);
        dataOutputStream.writeUTF(name);
        dataOutputStream.writeDouble(width);
        dataOutputStream.writeDouble(height);
        dataOutputStream.writeInt(nodeRef.length);

        for (Node node : nodeRef) {
            if (node instanceof FlowArea) {
                dataOutputStream.writeUTF("FlowArea");
            } else if (node instanceof Rectangle) {
                dataOutputStream.writeUTF("Rectangle");
            }
            node.save(dataOutputStream);
        }
        nextPosition = dataOutputStream.getFilePointer();
        dataOutputStream.seek(position);
        dataOutputStream.writeLong(nextPosition);
        dataOutputStream.seek(nextPosition);
    }

    @Override
    public void load(RandomAccessFile dataInputStream) throws IOException {
        nextPosition = dataInputStream.readLong();
        super.load(dataInputStream);
        name = dataInputStream.readUTF();
        width = dataInputStream.readDouble();
        height = dataInputStream.readDouble();
        int count = dataInputStream.readInt();
        nodeRef = new Node[count];
        for (int i = 0; i < count; i++) {
            String n = dataInputStream.readUTF();
            Node node = null;
            if ("FlowArea".equals(n)) {
                node = new FlowArea();
            } else if ("Rectangle".equals(n)) {
                node = new Rectangle();
            }
            nodeRef[i] = node;
            if (node != null) {
                node.load(dataInputStream);
            }
        }
        dataInputStream.seek(nextPosition);
    }
}
