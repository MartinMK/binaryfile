package binaryFile;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

class Main {
    public static void main(String[] args) throws IOException {

   /*     BinaryFile binaryFile = new BinaryFile();
        binaryFile.createBinaryFile();
        binaryFile.readBinaryFile();
*/

        List<Page> pageList = new ArrayList<Page>();
        List<Page> pageListToSave = new ArrayList<Page>();

        Page page = new Page("Page1", 210, 297, new Node[]{
                new FlowArea("FlowArea1", 10, 10, 80, 50, new String[]{"Petr Novak", "Jiraskova 213", "Brno", "12345"}),
                new FlowArea("FlowArea2", 120, 10, 80, 50, new String[]{"Moje firma", "Product manager", "Ing. Jana Novotna", "Hradecka 25", "Pardubice", "54332", "Tel: 123456789"}),
                new Rectangle("Rectangle1", 5, 70, 200, 2, new Color("Color", 0, 0, 0)),
                new FlowArea("FlowArea3", 10, 80, 190, 190, new String[]{"Line 1", "Line 2", "Line 3", "Line 4", "Line 5"}),
        });
        pageListToSave.add(page);

        RandomAccessFile raf = new RandomAccessFile(new File("data.bin"), "rw");
        raf.setLength(0);
        for (Page page1 : pageListToSave) {
            page1.save(raf);
        }

        raf.close();

        raf = new RandomAccessFile(new File("data.bin"), "rw");
        page = new Page();

        try {
            while (true) {
                page.load(raf);
                pageList.add(page);
                page = new Page();
            }
        } catch (EOFException ex) {
            System.out.println("end of file");
        }
        raf.close();
        for (Page page1 : pageList) {
            System.out.println(page1.toString());
        }
    }


}
